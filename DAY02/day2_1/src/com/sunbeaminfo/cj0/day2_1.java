package com.sunbeaminfo.cj0;
import java.util.*;


/*
class Employee
{
	private int id;
	private int salary;
	private String name;
	Scanner sc=new Scanner(System.in);
	void accept_record() // Employee object address will get stored into this 
	{
		System.out.println("Enter ID :");
		this.id=sc.nextInt();
		System.out.println("Enter Salary :");
		this.salary=sc.nextInt();
		System.out.println("Enter Name :");
		this.name=sc.next();
		
	}
	void print_record()
	{
		System.out.println("ID = "+this.id+" Salary = "+this.salary+" Name ="+this.name);
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + "]";
	}
	
	
}

public class day2_1 {

	public static void main(String[] args) 
	{
		Employee e1=new Employee();
		e1.accept_record(); 
		// accept_record() called upon e1 object 
		// this keyword (will hold current object address)
		//e1.print_record(); 
		Employee e2; // instance 
		e2=new Employee();// reference type
		e2.accept_record();
		//e2.print_record();
		
		System.out.println("Object Information : "+e1);
		// toString() function 
		// getClass().getName()+@+hashcode (address)
		System.out.println("Object Information : "+e2);
		
		
		//int a=50; //value type //stack 
		//Integer i=new Integer(125); //reference type 
		
		

	}

}
*/