package day2_3;

import java.util.Scanner;



/*


class Test
{
	private int num1;// non static field 
	private int num2; // non static field 
	private static int s; //static field
	
	
	public int getNum1() {
		return num1;
	}
	public void setNum1(int num1) {
		this.num1 = num1;
	}
	public int getNum2() {
		return num2;
	}
	public void setNum2(int num2) {
		this.num2 = num2;
	}
	public static int getS() {
		return s;
	}
	public static void setS(int s) {
		Test.s = s;
	}	
	
	
}
class day2_3
{
	public static void main(String args[])
	{
		Test t1=new Test(); // parameterless constructor 
		
		//Scanner sc=new Scanner(System.in);
		//int num1;
		//num1=sc.nextInt();
		//t1.setNum1(num1);
		
		t1.setNum1(50);
		t1.setNum2(60);
		Test.setS(70);
		
		
		System.out.println(t1.getNum1()); // accessing getter of non static field
		System.out.println(t1.getNum2());
		System.out.println(Test.getS());
		// accessing getter of static field 
		
		
	}
}

*/



/*
class Test
{
	private int num1;// non static field 
	private int num2; // non static field 
	private static int s; //static field 
	
	
	static Scanner sc=new Scanner(System.in);
	Test()
	{
		System.out.println("Inside Test Class Constructor");
		this.num1=10;
		this.num2=20;
	}
	//initiliaze static fields
	// static initializer block 
	static
	{
		System.out.println("Inside Static Initializer Block");
		System.out.println("Enter Number:  ");
		Test.s=sc.nextInt();
		//Test.s=400;
	}
	
	public void print_data()
	{
		System.out.println("Num1 : "+num1+" Num2 : "+num2+" S = "+s);
	}
}

class day2_3
{
	public static void main(String args[])
	{
		Test t1=new Test(); // parameterless constructor 
		t1.print_data();
		
	}
}

*/





/*
class Test
{
	public void sum(int num1,int num2) // non static member function
	{
		System.out.println("Addition:"+(num1+num2));
	}
	public void sub(int num1,int num2) // non static member function 
	{
		System.out.println("Subtraction :"+(num1-num2));
	}
	public static void mul(int num1,int num2) // static member function
	{
		System.out.println("Multiplication :"+(num1*num2));
	}
}

public class day2_3 
{
	public static void main(String[] args)
	{
			Test t1=new Test();
			t1.sum(50, 20); // instance methods 
			t1.sub(140, 20); //instance methods 
			//Test.sub(60,30); //annot make a static reference to the non-static method 
			t1.mul(5, 5); //ok
			Test.mul(4, 4); // ok 
	}
}
*/





/*
public class day2_3 
{
	
	public static void sum(int num1,int num2)
	{
		System.out.println("Addition:"+(num1+num2));
	}
	public static void sub(int num1,int num2)
	{
		System.out.println("Subtraction :"+(num1-num2));
	}
	public static void main(String[] args) 
	{
		//static member function can call other static functions 
		day2_3.sum(40,20); //cannot make a static reference to the non-static method
		day2_3.sub(150,70); //class methods / static methods 
	}

}


*/