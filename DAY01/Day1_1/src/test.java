// First Java Program 
// entry point function  : main()

class test
{
	// public : it is acccesssed outside its scope
	// static : we dont create object of the class
	//in which main is declared 
	// void  : return type 
	// main: keyword / entry point function
	// Command line arguments (String args[])
	public static void main(String args[])
	{
		System.out.println("Welcome you all ");
		System.out.println("for Core Java Foundation");
		
		// println() print line 
		// print the data on output screen
		//println() function is defined within PrintStream class
		// java.io.printStream
	}
}

// function within the class
//object call to function/method

// static
// static methods are called with the help of classname







//test.java
//test.class 
// .class files are equal to number of classes avaialable inside program



// VsCode
// Compilation : 
// javac test.java 
// Execute : 
// java test







