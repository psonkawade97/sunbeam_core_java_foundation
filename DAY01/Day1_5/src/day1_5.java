
// UNBOXING : NON PRIMITIVE TO PRIMITIVE 
// BOXING : PRIMITIVE TO NON PRIMITIVE 

public class day1_5 {

	public static void main(String[] args) 
	{
		int number=123;
		String str=Integer.toString(number);
		System.out.println("Str = "+str);
		
		int val=320;
		String str1=String.valueOf(val);
		System.out.println("Val = "+val);
		
		
		
	}
}




/*
int num=35;
double d;
d=num; //widening 
d=(double)num ; // widening 

*/


/*
//typecasting 
// narrowing and widening
 // double = 8 bytes
// double to int ==> narrowing 
// int to double ==> widening 

public class day1_5 {

	public static void main(String[] args) 
	{
		double d=50.5;
		double d1=d;
		int num=(int)d;//Type mismatch: cannot convert from double to int
		//narrowing 
		System.out.println("value of d "+d+" value of d1 "+d1);
		System.out.println("Num value = "+num);
	}

}
*/