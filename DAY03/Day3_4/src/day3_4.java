class Person
{
	String name;
	int age;
	Person()
	{
		System.out.println("Inside Person Parameterless Constructor");
		this.name="DEFAULT";
		this.age=1;
	}
	Person(String name,int age) // paramatrized constructor
	{
		System.out.println("Inside Person Paramatrized Constructor");
		this.name=name;
		this.age=age;
	}
	void disp_record()
	{
		System.out.println("Name  : "+name+" Age : "+age);
	}
}

class Employee extends Person
{
	int id;
	int salary;
	Employee()
	{
		System.out.println("Inside Employee Parameterless Constructor");
		this.id=1;
		this.salary=1;
	}
	Employee(String name,int age,int id,int salary)
	{
		super(name,age);
		//if we wish to give a call to paramatrized constructor
		//of super class
		//then in java we use super keyword
		
		System.out.println("Inside Employee Paramatrized Constructor");
		this.id=id;
		this.salary=salary;
	}
	
	void disp_record()
	{
		//if we wish to give a call to
		//overridden function of super class
		//then we use super keyword
		super.disp_record();
		System.out.println("ID : "+id+" Salary : "+salary);
	}
}

class day3_4
{
	public static void main(String args[])
	{
		Employee emp=new Employee("Akshita",33,1,60000);
		emp.disp_record();
	}
}


/*
class Person
{
	String name;
	int age;
	Person()
	{
		System.out.println("Inside Person Parameterless Constructor");
		this.name="DEFAULT";
		this.age=1;
	}
	Person(String name,int age) // paramatrized constructor
	{
		System.out.println("Inside Person Paramatrized Constructor");
		this.name=name;
		this.age=age;
	}
	void show_record()
	{
		System.out.println("Name  : "+name+" Age : "+age);
	}
}

class Employee extends Person
{
	int id;
	int salary;
	Employee()
	{
		System.out.println("Inside Employee Parameterless Constructor");
		this.id=1;
		this.salary=1;
	}
	Employee(String name,int age,int id,int salary)
	{
		super(name,age);
		//if we wish to give a call to paramatrized constructor
		//of super class
		//then in java we use super keyword
		
		System.out.println("Inside Employee Paramatrized Constructor");
		this.id=id;
		this.salary=salary;
	}
	
	void display_record()
	{
		this.show_record(); //will call to super class method
		System.out.println("ID : "+id+" Salary : "+salary);
	}
}

class day3_4
{
	public static void main(String args[])
	{
		Employee emp=new Employee("Akshita",33,1,60000);
		emp.display_record();
	}
}
*/


/*
 * 
 
class Person
{
	String name;
	int age;
	Person()
	{
		System.out.println("Inside Person Parameterless Constructor");
		this.name="DEFAULT";
		this.age=1;
	}
	void show_record()
	{
		System.out.println("Name  : "+name+" Age : "+age);
	}
}

class Employee extends Person
{
	int id;
	int salary;
	Employee()
	{
		System.out.println("Inside Employee Parameterless Constructor");
		this.id=1;
		this.salary=1;
	}
	
	void display_record()
	{
		this.show_record(); //will call to super class method
		System.out.println("ID : "+id+" Salary : "+salary);
	}
}

class day3_4
{
	public static void main(String args[])
	{
		//Person p=new Person(); // ok
		//p.show_record();
		//p.display_record();//method display_record() is undefined for the type Person
		
		Employee emp=new Employee();
		// a base class parameterless constructor is called
		// automatically by derived class parameteless constructor
		emp.display_record();
	}
}


*/