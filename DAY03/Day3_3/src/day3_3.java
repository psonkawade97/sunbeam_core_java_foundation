
class Date
{
	int day;
	int month;
	int year;
	public Date() {

	}
	public Date(int day, int month, int year) {
		
		this.day = day;
		this.month = month;
		this.year = year;
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	
}

class Employee
{
	int id;
	int salary;
	Date doj=new Date(); // ASSOCIATION 
	public Employee() 
	{
		this.id=0;
		this.salary=0;
		this.doj=new Date();
	}
	public Employee(int id, int salary, Date doj) {
	
		this.id = id;
		this.salary = salary;
		this.doj = doj;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	public Date getDoj() {
		return doj;
	}
	public void setDoj(Date doj) {
		this.doj = doj;
	}
	
	
	
	
}


public class day3_3 {

	public static void main(String[] args) 
	{
	
		Employee emp=new Employee(); // call to parameterless constructor
		emp.setId(20);
		emp.setSalary(70000);
		Date joinDate=new Date(3,9,2021);//call to paramatrized 
		//constructor of Date class
		emp.setDoj(joinDate);
		
		System.out.println("ID : "+emp.getId());
		System.out.println("Salary :"+emp.getSalary());
		joinDate=emp.getDoj(); // get the information about the current object
		System.out.println("Date : "+joinDate.getDay()+"-"+joinDate.getMonth()+"-"+joinDate.getYear());
		
		
		
		
	}

}
