import java.time.LocalDate;
import java.util.Arrays;
class Date
{
	private int day;
	private int month;
	private int year;
	
	public Date()
	{
		LocalDate ldt=LocalDate.now();
		//ldt will hold complete data of system 
		this.day=ldt.getDayOfMonth();
		this.month=ldt.getMonthValue();
		this.year=ldt.getYear();
	}

	@Override
	public String toString() {
		return "Date [day=" + day + ", month=" + month + ", year=" + year + "]";
	}
		
}

public class day3_2 
{
	public static void main(String args[])
	{
		 
		Date[] arr=new Date[3];//array of references
		for(int i=0;i<arr.length;i++)
			arr[i]=new Date();
		for(int i=0;i<arr.length;i++)
			System.out.println(arr[i].toString()); 
		
		
	}
}



/*
public class day3_2 
{
	public static void main(String args[])
	{
		//array of references 
		Date[] arr=new Date[3];
		System.out.println(Arrays.toString(arr));  //[null, null, null]
		
	}
}
*/
/*
public class day3_2 
{
	public static void main(String args[])
	{
		//array of references 
		Date[] arr=new Date[3];
		System.out.println(arr[0]); // null 
		System.out.println(arr[1]); // null 
		System.out.println(arr[2]); //null
	}
}

*/