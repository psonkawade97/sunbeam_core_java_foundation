

public class day3_5
{

	public static void main(String[] args)
	{
		String s1=new String("Sunbeam"); //char s1[]={'S','u','n','b'....}
		String s2=new String("Sunbeam");
		//if(s1==s2) //address 
			//System.out.println("Strings are equal");
		//else
		//	System.out.println("Strings are not equal");
		
		if(s1.equals(s2)) //contents 
			System.out.println("Strings are equal");
		else
			System.out.println("Strings are not equal");
		
	}
}

/*

public class day3_5
{

	public static void main(String[] args)
	{
		StringBuffer str=new StringBuffer("Sunbeam Infotech");
		System.out.println("string : "+str);
		str.append(" Pune ");
		System.out.println("string : "+str); // mutable 
		str.setCharAt(0, 's');
		System.out.println("string : "+str);
		
	}
}
*/
/*
public class day3_5
{

	public static void main(String[] args)
	{
		String str="Sunbeam Infotech"; // Immutable
		str.toLowerCase();
		System.out.println("Lower case : "+str);
		String s1=str.toLowerCase();
		System.out.println("Lower case : "+s1);
		System.out.println(s1.substring(3));
		System.out.println(s1.substring(3,7));
		System.out.println(s1.contains("sun")); //boolean
		
		String s2="Soon";
		String s3=s2.replace('S','M');
		System.out.println("S2 : "+s2+ " S3 : "+s3);
		
		String s4="Soon";
		String s5=s4.replace("Soon","Moon");
		System.out.println("S4 : "+s4+ " S5 : "+s5);
		
	}
}
*/

/*
public class day3_5
{

	public static void main(String[] args)
	{
		String name="Akshita";
		int len=name.length();
		System.out.println("Name : "+name+" Length : "+len);
		char ch=name.charAt(1);
		System.out.println("Character at 1st index : "+ch);
		
	}
}


*/


/*
public class day3_5 {

	public static void main(String[] args)
	{
		char str1[]= {'s','u','n','b','e','a','m'};
		String s=new String(str1);
		System.out.println("String Data : "+s);
		
		String s2="Infotech"; // String s2=new String("Infotech");
		System.out.println("S2 : "+s2);
		System.out.println(s+s2); //concatination
		
		
	}

}

*/
